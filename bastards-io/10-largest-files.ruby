require 'open-uri'

dir = '/home/sam/projects/**/*'

files = Dir.glob( dir ).sort_by! {|fname| File.size(fname)}

extensions = {}
files.each do |fname|
   ext = ""
   index = fname.rindex('.')
   if index != nil
      ext = fname[index+1..-1].downcase
   end

   if extensions.has_key?(ext)
      extensions[ ext ][:count] += 1
      extensions[ ext ][:size] += File.size( fname )
   else
      extensions[ ext ] = {}
      extensions[ ext ][:count] = 1
      extensions[ ext ][:size] = File.size( fname )
   end
end

maxlog = 0
File.open("output.tabs", "w" ) do |f|
   f.puts("Filetype\tCount\tBytes")
   extensions.sort_by{ |k,v| v[:size] }[-10,10].each do |k,v|
      log = Math.log10( v[:size] )
      puts "log of #{v[:size]} : #{log}"
      maxlog = log.to_i if log > maxlog
      f.puts("#{k}\t#{v[:count]}\t#{v[:size]}")
   end
end

ratios = ""
labels = ""
puts "maxlog: #{maxlog}"
puts "dividing by : #{(10**(maxlog-1))}"
extensions.sort_by{ |k,v| v[:size] }[-10,10].each do |k,v|
   pp v
   ratios += "#{(v[:size]/(10**(maxlog-1))).to_i}," 
   labels += "#{k}|"
end

ratios.chop!
labels.chop!

pp ratios
pp labels

# url = "https://chart.googleapis.com/chart?cht=p&chd=t:10,20,30,40&chs=500x300&chl=Jan|Feb|Mar|Apr"
url = "https://chart.googleapis.com/chart?cht=p&chd=t:#{ratios}&chs=500x300&chl=#{labels}"

pp url

lines = URI.open(url).readlines

File.open("chart", "w" ) do |f|
   lines.each do |line|
      f.print(line)
   end
end


