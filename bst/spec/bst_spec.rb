require_relative '../lib/tree.rb'

RSpec.describe 'Binary Search Tree' do

   let(:bst) { Tree.new }
   let(:in_arr_1)         { [3,2,4,5,1,1,6,6] }
   let(:in_arr_1_inorder) { [1,2,3,4,5,6] }
   let(:in_arr_2)         { [9] }

   describe 'BST build_tree tests' do
      context 'when given an unsorted array with duplicates' do
         it 'builds tree correctly' do
            mid = in_arr_1_inorder.length/2
            expect(bst.build_tree(in_arr_1).data).to eq(in_arr_1_inorder[mid])
         end
      end

      context 'when given an array of size 1' do
         it 'builds tree correctly' do
            expect(bst.build_tree(in_arr_2).data).to eq(in_arr_2[0])
            ret_arr = []
            bst.inorder() { |node| ret_arr.push(node.data) }
            expect(ret_arr).to eq(in_arr_2)
         end
      end
   end

end
