require_relative '../../merge_sort/merge'



class Node

   include Comparable

   attr_accessor :left, :right, :data

   def <=>(other)
      return 1 if other == nil
      data <=> other.data
   end

   def initialize( data, left=nil, right=nil )
      @data = data
      @left = left
      @right = right
   end

   def to_s()
      str = "(#{data})"
      return str
   end

   def replace( other )
      @data  = other.nil? ? nil : other.data
      @left  = other.nil? ? nil : other.left
      @right = other.nil? ? nil : other.right
   end
end




class Tree

   def initialize( )
      @root = nil
   end


   def to_s()
      str = ""
      node_levels = []
      stem_levels = []
      queue = Queue.new
      queue.push( @root )
      while not queue.empty? # loop over tree
         level = []
         stem_arr = []
         qLen = queue.length
         for i in (0...qLen) # loop over level
            stem_str = ""
            node = queue.pop()
            next if node == nil
            level.push( node )
            queue.push( node.left )
            stem_str += "/" if node.left != nil
            queue.push( node.right )
            stem_str += "\\" if node.right!= nil
            stem_arr.push( stem_str )
         end # level

         node_levels.push( level )
         stem_levels.push( stem_arr )

      end # tree

      # get width of tree
      width = 0
      node_levels.each{ |elem| width = elem.join().size() if elem.join().size() > width }
      str += "-"*width
      str += "\n"
      prev_first_stem_idx = 0
      # do final formatting
      node_levels.each_index do |index|
         level = node_levels[index]
         next if level.size() < 1
         gap = width / level.size()
         # print node level
         level.each_index do |jindex|
            str += level[jindex].to_s().center(gap)
         end
         str += "\n"

         # print stem level
         next if stem_levels[index].all? { |elem| elem == "" }

         stem_levels[index].each_index do |jindex|
            stem = stem_levels[index][jindex].center(gap)
            if stem != nil
               str += stem 
            end
         end
         str += "\n"
      end
      str += "-"*width
      str += "\n"

      return str
   end

   # returns the root node for a BST created from a SORTED array
   def __build_tree( data_arr )
      if data_arr.size == 1
         root = Node.new( data_arr[0] )
         return root
      elsif data_arr.size == 0
         return nil
      end

      midpoint = data_arr.size() / 2

      root = Node.new( data_arr[midpoint] )

      root.left  = __build_tree( data_arr[0...midpoint]   )
      root.right = __build_tree( data_arr[midpoint+1..-1] )
      return root
   end


   def build_tree( data_arr )
      arr = remove_duplicates( data_arr )

      sorted_arr = merge_sort( arr )
      
      @root = __build_tree( sorted_arr )
      return @root
   end

   def insert( data, root=@root )
      return nil if root.data == data
      if root.data > data
         if root.left == nil
            root.left = Node.new( data )
         else
            insert( data, root.left )
         end
      else
         if root.right == nil
            root.right = Node.new( data )
         else
            insert( data, root.right)
         end
      end
   end

   # if not given block, prints nodes inorder, else yields nodes
   # left -> root -> right
   def inorder( root=@root, &block )
      # left 
      if root.left != nil
         inorder( root.left         ) if not block_given?
         inorder( root.left, &block ) if block_given?
      end
      
      # root
      print root         if not block_given?
      block.call( root ) if block_given? 

      # right
      if root.right != nil   
         inorder( root.right         ) if not block_given?
         inorder( root.right, &block ) if block_given?
      end
   end

   # if not given block, prints nodes in preorder, else yields nodes
   # root -> left -> right
   def preorder( root=@root, &block )
      # root
      block.call( root ) if block_given?
      print root         if not block_given?

      # left
      if root.left != nil
         preorder( root.left         ) if not block_given?
         preorder( root.left, &block ) if block_given?
      end

      # right
      if root.right != nil
         preorder( root.right         ) if not block_given?
         preorder( root.right, &block ) if block_given?
      end
   end

   # if not given block, print nodes in postorder, else yields nodes
   # left -> right -> root
   def postorder( root=@root, &block )
      # left 
      if root.left != nil
         postorder( root.left         ) if not block_given?
         postorder( root.left, &block ) if block_given?
      end

      # right 
      if root.right != nil
         postorder( root.right         ) if not block_given?
         postorder( root.right, &block ) if block_given?
      end
      
      # root
      block.call( root ) if block_given?
      print root         if not block_given?
   end
   
   # if not given block, prints nodes in levelorder, else yields nodes
   # BFS use queue
   def levelorder( root=@root, &block )
      queue = Queue.new
      queue.push( root )
      while not queue.empty?
         level = []
         qLen = queue.length()
         for i in (0...qLen)
            node = queue.pop()
            next if node == nil

            block.call( node ) if block_given?
            print node         if not block_given?

            queue.push( node.left )
            queue.push( node.right )
         end
      end
   end

   def delete( data, root=@root )
      return nil if root == nil
      if data < root.data
         delete( data, root.left )
      elsif data > root.data
         delete( data, root.right )
      else # data == root.data
         # no children
         if root.left == nil and root.right == nil
            inorder_succ = nil
            inorder( @root ) do |node| 
               if node > root 
                  inorder_succ = node 
                  break
               end
            end
            inorder_succ.left =  nil if inorder_succ.left == root
            inorder_succ.right = nil if inorder_succ.right == root

         # one child
         elsif root.right == nil or root.left == nil
            root.replace( root.right ) if root.right != nil
            root.replace( root.left  ) if root.left != nil
         # two children
         else
            inorder_succ = nil
            prev_node = nil
            parent = 0
            inorder( @root ) do |node| 
               if parent == 1
                  prev_node = node
                  break
               end 
               if node > root 
                  inorder_succ = node 
                  parent = 1
               end
            end

            # remove tree ref to successor
            prev_node.left = nil if prev_node.left == inorder_succ
            prev_node.right = nil if prev_node.right == inorder_succ

            root.data = inorder_succ.data # move successor data to root
            inorder_succ =  nil           # remove last ref to successor
         end
      end
   end

   def find( data, root=@root )
      return nil  if root.nil?
      return root if root.data == data
      if root.data > data
         find( data, root.left )
      else
         find( data, root.right )
      end
   end

   # returns number of edges in longest path from node to a leaf node
   def height( node, height=-1 )
      return height if node.nil?
      left_height  = height( node.left, height+1 )
      right_height = height( node.right, height+1 )
      return left_height > right_height ? left_height : right_height
   end

   # returns number of edges in path from a given node to the tree's root node
   def depth( node, root=@root, height=0 )
      return nil if root.nil?
      return height if root == node
      if root > node
         return depth( node, root.left, height+1 )
      else
         return depth( node, root.right, height+1 )
      end
   end

   # is one where the difference between heights of left subtree and right 
   #  subtree of every node is not more than 1
   def balanced?( root=@root )
      self.levelorder() do |node|
         left_height  = self.height(node.left)
         right_height = self.height(node.right)
         diff = left_height - right_height
         if diff.abs() > 1
            return false
         end
      end
      return true
   end

   def rebalance( root=@root )
      new_arr = []
      self.inorder( root ) do |node|
         new_arr.push( node.data )
      end
      return build_tree( new_arr )
   end

   # taken from https://www.theodinproject.com/lessons/ruby-binary-search-trees
   def pretty_print(node = @root, prefix = '', is_left = true)
     pretty_print(node.right, "#{prefix}#{is_left ? '│   ' : '    '}", false) if node.right
     puts "#{prefix}#{is_left ? '└── ' : '┌── '}#{node.data}"
     pretty_print(node.left, "#{prefix}#{is_left ? '    ' : '│   '}", true) if node.left
   end


end


def do_test()
   # array = [1, 7, 4, 23, 8, 9, 4, 3, 5, 7, 9, 67, 6345, 324]
   array = [1, 2, 3, 4, 5, 6]

   tree = Tree.new()

   tree.build_tree( array )

   print tree

   puts "balanced: #{tree.balanced?}"

   tree.insert( 18 )
   tree.insert( 322 )

   print tree

   puts "balanced: #{tree.balanced?}"

   puts "INORDER"
   tree.inorder() { |node| puts node }
   puts

   # puts "PREORDER"
   # tree.preorder() { |node| puts node }
   # puts

   # puts "POSTORDER"
   # tree.postorder() { |node| puts node }
   # puts

   # puts "LEVELORDER"
   # tree.levelorder() { |node| puts node }
   # puts

   tree.delete( 4 )
   print tree
   puts "balanced: #{tree.balanced?}"

   tree.delete( 1 )
   print tree
   puts "balanced: #{tree.balanced?}"

   tree.rebalance()
   print tree
   puts "balanced: #{tree.balanced?}"

   puts tree.height( tree.find( 5 ) )
   puts tree.height( tree.find( 322 ) )

   puts tree.depth( tree.find( 5 ) )
   puts tree.depth( tree.find( 322 ) )
end

def do_other_test()
   arr = Array.new(15) { rand(1..100) }
   tree = Tree.new()
   tree.build_tree( arr )
   puts tree

   puts tree.balanced?()
   tree.levelorder()
   puts
   tree.preorder()
   puts
   tree.postorder()
   puts
   tree.inorder()
   puts

   tree.insert( 900 )
   tree.insert( 103 )
   tree.insert( 153 )

   puts tree.balanced?()
   puts tree
   tree.rebalance()
   puts tree
   tree.levelorder()
   puts
   tree.preorder()
   puts
   tree.postorder()
   puts
   tree.inorder()
   puts

   tree.pretty_print()
   

end

# do_test()
# do_other_test()


