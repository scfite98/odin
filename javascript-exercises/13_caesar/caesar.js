function isAlpha( str ) {
   return /^[a-zA-Z]+$/.test(str);
}

// a-z : 97-122
function getShiftedCharCode( charCode, shift ) {
   let count = 0;
   let move = (shift < 0) ? -1 : 1;
   while ( count < Math.abs(shift) ) {
      if ( charCode == 97 && move == -1 ) {
         charCode = 122;
      }
      else if ( charCode == 122 && move == 1){
         charCode = 97;
      }
      else{
         charCode += move;
      }
      count++;
   }
   return charCode;
}

const caesar = function( str, shift ) {
   let out = "";
   let lowerStr = str.toLowerCase(); 

   for ( let i = 0; i < str.length; i++ ){
      if ( !isAlpha( str.charAt( i ) ) ){
         out = out.concat( str.charAt( i ) );
         continue;
      }

      let charCode    = lowerStr.charCodeAt( i );
      let shiftedCode = getShiftedCharCode( charCode, shift );

      if ( charCode === str.charCodeAt( i ) ) // was original char lower?
         out = out.concat( String.fromCharCode( shiftedCode ) );
      else // original was upper case
         out = out.concat( String.fromCharCode( shiftedCode ).toUpperCase() );
   }

   return out;
};

// Do not edit below this line
module.exports = caesar;
