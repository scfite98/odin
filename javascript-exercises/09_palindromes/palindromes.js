function isAlpha( str ) {
   return /^[a-zA-Z]+$/.test(str);
}

function removeNonAlpha( str ) {
   for ( let i = 0; i < str.length; i ++ )
   {
      if ( !isAlpha( str.charAt( i ) ) )
      {
         let end = str.slice( i+1 );
         str = str.slice( 0, i );
         str = str.concat( end );
         i--;
      }
   }
   return str;
}

const palindromes = function ( str ) {
   str = str.toLowerCase();
   str = removeNonAlpha( str ); 
   let copy = "";
   for ( let i = str.length; i >= 0; i-- )
   {
      copy = copy.concat( str.charAt( i ) );
   }
   if ( copy === str )
      return true;
   else
      return false;
};

// Do not edit below this line
module.exports = palindromes;
