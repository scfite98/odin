const sumAll = function( a, b ) {
   if ( a < 0 || b < 0 || typeof( a ) !== "number" || typeof( b ) !== "number" ) return "ERROR";
   let sum = 0;
   let tmp = 0;
   if ( a > b ) {
      tmp = a;
      a = b;
      b = tmp;
   }
   for ( let i = a; i <= b; i++ )
      sum += i;

   return sum;
};

// Do not edit below this line
module.exports = sumAll;
