const ftoc = function( temp ) {
   c = (5/9)*(temp-32);
   return (Math.round(c*10)/10);
};

const ctof = function( temp ) {
   f = ((9/5) * temp) + 32;
   return (Math.round(f*10)/10);
};

// Do not edit below this line
module.exports = {
  ftoc,
  ctof
};
