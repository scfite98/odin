const findTheOldest = function( arr ) {
   return arr.reduce( (prev, curr) => {
         let pAge = prev['yearOfDeath'] || 2022;
         pAge -= prev['yearOfBirth'];

         let cAge = curr['yearOfDeath'] || 2022;
         cAge -= curr['yearOfBirth'];

         if ( cAge > pAge ){
            prev = null;
            return curr;
         }
         else{
            curr = null;
            return prev;
         }
         })
};

// Do not edit below this line
module.exports = findTheOldest;
