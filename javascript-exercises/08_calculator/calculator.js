const add = function( a, b ) {
   return a + b;	
};

const subtract = function( a, b ) {
   return a - b;	
};

const sum = function( arr ) {
   let s = 0;	
   for ( let i = 0; i < arr.length; i++ )
      s += arr[i];
   return s;
};

const multiply = function( list ) {
   let acc = 1;
   for( i = 0; i < list.length; i++ )
      acc *= list[i];
   return acc;
};

const power = function( a, b ) {
   return a ** b;
};

const factorial = function( num ) {
   if ( num === 0 ) return 1;
   let fac = num;	
   num--;
   while( num > 0 )
   {
      fac *= num; 
      num--;
   }
   return fac;
};

// Do not edit below this line
module.exports = {
  add,
  subtract,
  sum,
  multiply,
  power,
  factorial
};
