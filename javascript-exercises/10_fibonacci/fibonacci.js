
const fibonacci = function( num, count=1, cur=1, prev=0 ) {
   // console.log( num + ", " + count + ", " + cur + ", " + prev );
   if ( num < 0 ) return "OOPS";
   if ( num == count ) return cur;
   return fibonacci( num, count+1, cur+prev, cur );
};

// Do not edit below this line
module.exports = fibonacci;
