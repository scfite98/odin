# frozen_string_literal: true

require_relative 'board.rb'

class Game

   attr_reader :board

   def initialize()
      @player_one = "X"
      @player_two = "O"
      @current_player = @player_one
      @board = Board.new( 7, 6, "-" )
      @game_over = false
   end


   def play()
      until @game_over do
         turn = self.get_turn()
         @board.add_turn( turn, @current_player )
         @board.show()
         @game_over = @board.game_over?()
         if @game_over
            break
         end
         self.set_new_current_player()
      end
   end

private

   def set_new_current_player()
      if @current_player == @player_one
         @current_player = @player_two
      else
         @current_player = @player_one
      end
   end

   def get_turn()
      while true do
         puts "please enter a number between 1 and 7"
         turn = gets.chomp.to_i
         turn = turn - 1
         if @board.valid_turn?( turn )
            return turn
         else
            puts "turn is invalid"
            @board.show()
         end
      end
   end

end


