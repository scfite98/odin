# frozen_string_literal: true


class Board

   attr_reader :board

   def initialize( width, height, init_char )
      @init_char = init_char
      @width = width
      @height = height
      @board = Array.new( width ) {
         Array.new( height, @init_char )
      }
   end

   def get_board_arr()
      return @board
   end

   def game_over?()
      # do a dfs?
      # look for a player char,
      #  when found, 
      #  look in each direction (updown, leftright, diag ) for n in a row

      for row in 0...@board.size() do
         for col in 0...@board[row].size() do
            if board[row][col] != @init_char

               max_score = get_max_consec( row, col )

               if max_score >= 4
                  return true
               end
            end
         end
      end

      return false
   end

   def show()
      pp @board
   end

   # call valid turn before me
   def add_turn( turn, player )
      self.update_board( turn, player )
   end


   def valid_turn?( turn )
      if turn >= 0 and turn < @width
         return @board[turn][@height-1] == @init_char
      else
         return false
      end
   end

private

   def get_max_consec( row, col )
      player = @board[row][col]
      row_copy = row
      col_copy = col
      #  look in each direction (updown, leftright, diag ) for n in a row
      num_consec = 1

      # look left
      while row_copy > 0 
         row_copy -= 1
         if board[row_copy][col_copy] == player
            num_consec += 1
         else
            break
         end
      end
      row_copy = row

      # look right
      while row_copy < @width - 1
         row_copy += 1
         if board[row_copy][col_copy] == player
            num_consec += 1
         else
            break
         end
      end
      row_copy = row

      num_consec = 1
      # look down
      while col_copy > 0
         col_copy -= 1
         if board[row_copy][col_copy] == player
            num_consec += 1
         else
            break
         end
      end
      col_copy = col

      # look up
      while col_copy < @height - 1
         col_copy += 1
         if board[row_copy][col_copy] == player
            num_consec += 1
         else
            break
         end
      end
      col_copy = col


      return num_consec
   end

   def update_board( turn, player )
      idx = @board[ turn ].index( @init_char )
      @board[ turn ][ idx ] = player
   end

end
