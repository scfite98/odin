# frozen_string_literal: true

require_relative '../lib/board.rb'

# Here is a summary of what should be tested
   # 1. Command Method -> Test the change in the observable state
   # 2. Query Method -> Test the return value
   # 3. Method with Outgoing Command -> Test that a message is sent
   # 4. Looping Script Method -> Test the behavior of the method
   

# There are a handful of methods that you do not need to test:
   # 1. You do not have to test #initialize if it is only creating instance
   # variables. However, if you call methods inside the initialize method, you
   # might need to test #initialize and/or the inside methods. In addition, you
   # will need to stub any inside methods because they will be called when you
   # create an instance of the class.
   # 2. You do not have to test methods that only contain 'puts' or 'gets' 
   # because they are well-tested in the standard Ruby library.
   # 3. Private methods do not need to be tested because they should have test
   # coverage in public methods. However, as previously discussed, you may have
   # some private methods that are called inside a script or looping script method;
   # these methods should be tested publicly.

describe Board do

   width = 7
   height = 6
   init_char = "-"

   subject( :board ) do
      described_class.new( width, height, init_char )
   end

   # 1. Command Method -> Test the change in the observable state
   describe '#add_turn?' do

      context 'When given one turn' do
         it 'updates the board representation' do
            turn = 0
            player = "X"

            board.add_turn( turn, player )

            arr = board.get_board_arr()
            expect( arr[0][0] ).to eq( player )
         end
      end

      context 'When given two turns in one column' do
         it 'updates the board representation' do
            turn = 0
            player = "X"

            board.add_turn( turn, player )
            board.add_turn( turn, player )

            arr = board.get_board_arr()
            expect( arr[0][0] ).to eq( player )
            expect( arr[0][1] ).to eq( player )
         end
      end


      context 'When given two turns in different columns' do
         it 'updates the board representation' do
            turn = 0
            player = "X"

            board.add_turn( turn, player )
            board.add_turn( turn+1, player )

            arr = board.get_board_arr()
            expect( arr[0][0] ).to eq( player )
            expect( arr[1][0] ).to eq( player )
         end
      end

   end

   # 2. Query Method -> Test the return value
   describe '#valid_turn?' do

      context 'When there is room on the board' do
         it 'returns true' do
            turn = 0
            test_board = [[init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char]]

            board.instance_variable_set( :@board, test_board )

            expect( board.valid_turn?( turn ) ).to eq( true )
         end
      end

      context 'When there is no room on the board' do
         it 'returns false' do
            turn = 0
            test_board = [['X',        'X',        'X',        'X',        'X',        'X'      ],
                          [init_char,  init_char,  init_char,  init_char,  init_char,  init_char],
                          [init_char,  init_char,  init_char,  init_char,  init_char,  init_char],
                          [init_char,  init_char,  init_char,  init_char,  init_char,  init_char],
                          [init_char,  init_char,  init_char,  init_char,  init_char,  init_char],
                          [init_char,  init_char,  init_char,  init_char,  init_char,  init_char],
                          [init_char,  init_char,  init_char,  init_char,  init_char,  init_char]]

            board.instance_variable_set( :@board, test_board )

            expect( board.valid_turn?( turn ) ).to eq( false )
         end
      end

      context 'When there is one spot left on the board' do
         it 'returns true' do
            turn = 0
            test_board = [['X', 'X', 'X', 'X', 'X', init_char ],
                          ['X', 'X', 'X', 'X', 'X', 'X'],
                          ['X', 'X', 'X', 'X', 'X', 'X'],
                          ['X', 'X', 'X', 'X', 'X', 'X'],
                          ['X', 'X', 'X', 'X', 'X', 'X'],
                          ['X', 'X', 'X', 'X', 'X', 'X'],
                          ['X', 'X', 'X', 'X', 'X', 'X']]

            board.instance_variable_set( :@board, test_board )

            expect( board.valid_turn?( turn ) ).to eq( true )
         end
      end

      context 'When the turn is out of bounds' do
         it 'returns false' do
            bad_turn1 = -1
            bad_turn2 = width + 1

            test_board = [[init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char]]

            board.instance_variable_set( :@board, test_board )

            expect( board.valid_turn?( bad_turn1 ) ).to eq( false )
            expect( board.valid_turn?( bad_turn2 ) ).to eq( false )
         end
      end

   end

   # 2. Query Method -> Test the return value
   describe '#game_over?' do

      context 'When the board is empty' do
         it 'returns false' do
            test_board = [[init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char]]

            board.instance_variable_set( :@board, test_board )

            expect( board.game_over?() ).to eq( false )
         end
      end

      context 'When the board has a vertical win' do
         it 'returns true' do
            test_board = [[init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          ["X", "X", "X", "X", "X", "X"],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char]]

            board.instance_variable_set( :@board, test_board )

            expect( board.game_over?() ).to eq( true )
         end
      end

      context 'When the board has a horizontal win' do
         it 'returns true' do
            test_board = [["X", init_char, init_char, init_char, init_char, init_char],
                          ["X", init_char, init_char, init_char, init_char, init_char],
                          ["X", init_char, init_char, init_char, init_char, init_char],
                          ["X", init_char, init_char, init_char, init_char, init_char],
                          ["X", init_char, init_char, init_char, init_char, init_char],
                          ["X", init_char, init_char, init_char, init_char, init_char],
                          ["X", init_char, init_char, init_char, init_char, init_char]]

            board.instance_variable_set( :@board, test_board )

            expect( board.game_over?() ).to eq( true )
         end
      end

      context 'When the board has a diagonal win' do
         it 'returns true' do
            test_board = [[init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, init_char, init_char, init_char],
                          [init_char, init_char, init_char, "X", init_char, init_char],
                          [init_char, init_char, "X", init_char, init_char, init_char],
                          [init_char, "X", init_char, init_char, init_char, init_char],
                          ["X", init_char, init_char, init_char, init_char, init_char]]

            board.instance_variable_set( :@board, test_board )

            expect( board.game_over?() ).to eq( true )
         end
      end


   end
end
