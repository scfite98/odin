# frozen_string_literal: true

require_relative '../lib/game.rb'

# Here is a summary of what should be tested
   # 1. Command Method -> Test the change in the observable state
   # 2. Query Method -> Test the return value
   # 3. Method with Outgoing Command -> Test that a message is sent
   # 4. Looping Script Method -> Test the behavior of the method
   

# There are a handful of methods that you do not need to test:
   # 1. You do not have to test #initialize if it is only creating instance
   # variables. However, if you call methods inside the initialize method, you
   # might need to test #initialize and/or the inside methods. In addition, you
   # will need to stub any inside methods because they will be called when you
   # create an instance of the class.
   # 2. You do not have to test methods that only contain 'puts' or 'gets' 
   # because they are well-tested in the standard Ruby library.
   # 3. Private methods do not need to be tested because they should have test
   # coverage in public methods. However, as previously discussed, you may have
   # some private methods that are called inside a script or looping script method;
   # these methods should be tested publicly.

# board is 7x6

describe Game do
   subject(:game) { described_class.new() }

   before(:each) do
      game.instance_variable_set( :@board, instance_double(Board) )
   end
    
   # Command - Changes the observable state, but does not return a value.
   describe '#play' do

      context 'When called with no arguments' do
         it 'prompts the user for a turn' do
            allow( game.board ).to receive( :add_turn )
            allow( game.board ).to receive( :show )
            allow( game.board ).to receive( :game_over? ).and_return( true )

            expect( game ).to receive( :get_turn )
            game.play()
         end
      end

      it 'alternates the current player' do
            allow( game.board ).to receive( :add_turn )
            allow( game.board ).to receive( :show )
            allow( game.board ).to receive( :game_over? ).and_return( false, true )
            allow($stdin).to receive(:gets).and_return('0','0')

            expect( game ).to receive( :get_turn )
            expect( game ).to receive( :get_turn )
            expect( game.instance_variable_get( :@current_player ) ).to eq( "X" )


            game.play()

            expect( game.instance_variable_get( :@current_player ) ).to eq( "O" )
      end
   end

   describe '#get_turn' do

      context 'When called with no arguments' do
         it 'prompts the user for a turn' do
            allow( game.board ).to receive( :add_turn )
            allow( game.board ).to receive( :show )
            allow( game.board ).to receive( :game_over? ).and_return( true )

            expect( game ).to receive( :get_turn )
            game.play()
         end
      end
   end



end
