

class Node
   attr_accessor :value, :next

   def initialize( _value, _next=nil )
      @value = _value
      @next = _next
   end

   def to_s()
      return "( #{value} )"
   end

end

class LinkedList
   def initialize()
      @size = 0
      @head = nil
   end

   def append( value )
      if @head == nil
         @head = Node.new( value )
         @size = 1
      else
         cur = @head
         while cur.next != nil do
            cur = cur.next
         end
         cur.next = Node.new( value )
         @size += 1
      end
   end

   def prepend( value )
      if @head == nil
         @head = Node.new( value )
         @size = 1
      else
         old_head = @head
         @head = Node.new( value )
         @head.next = old_head
         @size += 1
      end
   end

   def size()
      return @size
   end

   def head()
      return @head
   end

   def tail()
      if @size < 2
         return @head 
      else
         cur = @head
         while cur.next != nil do
            cur = cur.next
         end
         return cur
      end

   end

   def at( index )
      i = 0
      cur = @head
      while cur.next != nil and i < index do
         cur = cur.next
         i += 1
      end
      return cur
   end

   def pop()
      return nil if size == 0
      cur = @head
      prev = cur
      while cur.next != nil do
         prev = cur
         cur = cur.next
      end
      prev.next = nil
      return cur
   end

   def contains?( value )
      ret = false
      cur = @head

      ret = true if cur.value == value
      while cur.next != nil do
         cur = cur.next
         ret = true if cur.value == value
      end

      return ret
   end

   def find( value )
      found = false
      i = 0
      cur = @head
      found = true if cur.value == value
      while cur.next != nil and not found do
         cur = cur.next
         found = true if cur.value == value
         i += 1
      end
      return i if found
      return nil if not found
   end

   #( value ) -> ( value ) -> ( value ) -> nil
   def to_s()
      return "( nil )" if size == 0 
      str = ""
      cur = @head
      while cur.next != nil do
         str += "( #{cur.value} ) -> "
         cur = cur.next
      end
      str += "( #{cur.value} ) -> "
      str += " nil "
      return str
   end

   def insert_at( value, index )
      i = 0
      cur = @head
      prev = cur
      while cur.next != nil and i < index do
         prev = cur
         cur = cur.next
         i += 1
      end
      return nil if i < index
      new = Node.new( value )
      prev.next = new
      new.next = cur
      return i
   end

   def remove_at( index )
      i = 0
      cur = @head
      prev = cur
      while cur.next != nil and i < index do
         prev = cur
         cur = cur.next
         i += 1
      end
      return nil if i < index
      nextN = cur.next
      cur.next = nil
      prev.next = nextN
      return i
   end
end



puts "test 1"
list = LinkedList.new()
list.append( 5 )
list.append( 3 )
list.append( 2 )
puts list
list = nil

puts "test 2"
list = LinkedList.new()
list.prepend( 9 )
list.prepend( 8 )
list.prepend( 7 )
puts list
puts "test 2.5"
puts "size: #{list.size}"
list = nil

puts "test 3"
list = LinkedList.new()
list.append( 5 )
list.append( 3 )
list.append( 2 )
list.append( 1 )
puts list
puts "head: #{list.head()}"
puts "tial: #{list.tail()}"
puts "test 3.5"
puts "list.at(1) #{list.at(1)}"
list = nil

puts "test 4"
list = LinkedList.new()
list.append( 5 )
list.append( 3 )
list.append( 2 )
list.append( 1 )
puts list
puts "list.pop() #{list.pop()}"
puts list
list = nil

puts "test 5"
list = LinkedList.new()
list.append( 5 )
list.append( 3 )
list.append( 2 )
list.append( 1 )
puts list
puts "list.contains?( 0 ) #{list.contains?( 0 )}"
puts "list.contains?( 1 ) #{list.contains?( 1 )}"
list = nil


puts "test 6"
list = LinkedList.new()
list.append( 5 )
list.append( 3 )
list.append( 2 )
list.append( 1 )
puts list
puts "list.find( 0 ) #{list.find( 0 )}"
puts "list.find( 1 ) #{list.find( 1 )}"
puts "list.find( 3 ) #{list.find( 3 )}"
list = nil

puts "test 7"
list = LinkedList.new()
list.append( 5 )
list.append( 3 )
list.append( 2 )
list.append( 1 )
puts list
puts "list.insert_at( 2, 9 ) #{list.insert_at( 2, 9 )}"
puts list
puts "list.insert_at( 9, 2 ) #{list.insert_at( 9, 2 )}"
puts list
list = nil

puts "test 8"
list = LinkedList.new()
list.append( 5 )
list.append( 3 )
list.append( 2 )
list.append( 1 )
puts list
puts "list.remove_at( 9 ) #{list.remove_at( 9 )}"
puts list
puts "list.remove_at( 2 ) #{list.remove_at( 2 )}"
puts list
list = nil
