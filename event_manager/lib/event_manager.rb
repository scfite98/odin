require 'google/apis/civicinfo_v2'
require 'csv'
require 'erb'
require 'time'
require 'date'



def clean_zipcode(zipcode)
   zipcode.to_s.rjust(5,'0')[0..4]
end

def clean_phone_number(number)
   number = number.to_s
   number.delete!('^0-9')
   if number.length == 10
      number
   elsif number.length < 10 or number.length > 11
      '0000000000'
   elsif number.length == 11
      if number[0] != '1'
         '0000000000'
      else
         number[1..-1]
      end
   end
end

def legislators_by_zipcode(zipcode)
   civic_info = Google::Apis::CivicinfoV2::CivicInfoService.new
   civic_info.key = 'AIzaSyClRzDqDh5MsXwnCWi0kOiiBivP6JsSyBw'

   begin
      civic_info.representative_info_by_address(
        address: zipcode.to_s,
       levels: 'country',
       roles: ['legislatorUpperBody', 'legislatorLowerBody']
      ).officials
  rescue
     'You can find your representatives by visiting www.commoncause.org/take-action/find-elected-officials'
  end
end


def save_thank_you_letter(id,form_letter)
   Dir.mkdir('output') unless Dir.exist?('output')

   filename = "output/thanks_#{id}.html"

   File.open(filename, 'w') do |file|
     file.puts form_letter
   end
end

contents = CSV.open(
  'event_attendees.csv',
  headers: true,
  header_converters: :symbol
)

template_letter = File.read('form_letter.erb')
erb_template = ERB.new template_letter

reg_hours = {}
reg_days = {}

contents.each do |row|
   id = row[0]
   name = row[:first_name]

   zipcode = clean_zipcode(row[:zipcode])

   phone_number = clean_phone_number(row[:homephone])

   datetime = row[:regdate].split(" ")
   date_str = datetime[0]
   time_str = datetime[1]

   time = Time.strptime( time_str, "%k:%M")
   date = Date.strptime( date_str, "%m/%d/%y")

   reg_hours[time.hour] = 0 if not reg_hours.has_key?( time.hour )
   reg_hours[time.hour] += 1

   reg_days[date.wday] = 0 if not reg_days.has_key?( date.wday )
   reg_days[date.wday] += 1 

   legislators = legislators_by_zipcode(zipcode)

   form_letter = erb_template.result(binding)

   save_thank_you_letter(id,form_letter)
end

p reg_hours
p reg_days
max_hour = reg_hours.key(reg_hours.values.max)
puts "peak hour: #{max_hour}"
max_day = reg_days.key(reg_days.values.max)
puts "peak day: #{Date::DAYNAMES[max_day]}"
