# frozen_string_literal: true

require_relative 'display.rb'

# Contains the logic to play the game
class Game
  include Display
  attr_reader :first_player, :second_player, :board, :current_player

  def initialize( rounds )
    @first_player = nil
    @second_player = nil
    @current_player = nil
    @num_rounds = rounds
    @current_round = 1
  end


  def play
    game_set_up 
    @current_player = first_player
    while @num_rounds >= @current_round
      round_set_up( @current_player )
      board.show
      player_turns
      round_conclusion
      @current_round += 1
      @current_player.guesser = !@current_player.guesser
      @current_player = switch_current_player
      @current_player.guesser = !@current_player.guesser
    end
    game_conclusion
  end

  def create_player(number)
    puts display_name_prompt(number)
    name = gets.chomp
    human = human_input()
    guesser = (number == 1) ? false : true
    Player.new(name, human, guesser)
  end

  def create_code( player )
    code = nil
    if player.human
      puts display_code_prompt( board.num_colors, board.code_len )
      code = gets.chomp
      if board.valid_guess?(code)
        board.code = code.split('') 
      else
        display_input_warning
        create_code(player)
      end
    else
      board.code.map! { |x| rand(0..board.num_colors-1) }
    end
    board.code.freeze
  end

  def turn(player)
    if player.human
      guess = turn_input(player)
    else
      guess = Array.new( board.code_len ){ rand(0..board.num_colors-1) }
      guess = guess.join()
    end
    board.update_board(guess)
    board.show
  end


  def game_set_up
    puts display_intro
    @first_player = create_player(1)
    @second_player = create_player(2)
    puts @first_player.name
    puts @second_player.name
  end

  def round_set_up( player )
    @board = Board.new
    puts "new board"
    p @board
    create_code( player )
  end

  private

  def human_input()
    puts display_human_prompt
    input = gets.chomp.downcase
    return true if input.match?(/^[y]$/)
    return false if input.match?(/^[n]$/)

    puts display_input_warning
    human_input()
  end

  def player_turns
    until board.full?
      if current_player.guesser
        turn(current_player) 
        other = switch_current_player
        other.score += 1
        break if board.game_won?
      end

      @current_player = switch_current_player
    end
  end

  def turn_input(player)
    puts display_player_turn(player.name, board.code_len, board.num_colors)
    guess = gets.chomp
    return guess if board.valid_guess?(guess)

    puts display_input_warning
    turn_input(player)
  end

  def switch_current_player
    if current_player == first_player
      second_player
    else
      first_player
    end
  end

  def game_conclusion
    if @first_player.score == @second_player.score
      puts display_game_tie(@first_player, board.code, board.board_width )
    else
      @current_player = switch_current_player if @current_player.score < switch_current_player.score
      puts display_game_winner(@current_player, board.code, board.board_width)
    end
  end

  def round_conclusion
    if board.game_won?
      # make sure guesser is displayed on victory
      @current_player = switch_current_player if !@current_player.guesser
      puts display_round_winner(@current_player, board.code, board.board_width, @current_round )
    else
      puts display_round_winner(@current_player, board.code, board.board_width, @current_round )
    end

  end
end

