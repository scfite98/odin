require 'colorize'

class Board
   attr_reader :code_len, :num_colors, :board_width
   attr_accessor :code

   def initialize( num_colors: 7, code_len: 4, num_guesses: 12 )
      @num_colors = num_colors
      @code_len = code_len
      @num_guesses = num_guesses
      @code = Array.new( @code_len )
      @board_width = 34

      # holds players guesses
      @guess_arr = Array.new( @num_guesses ) { Array.new( @code_len ) }
      # holds matches per guess
      @match_arr = Array.new( @num_guesses ) { Array.new( @code_len ) }
   end

  def show()
    num_map = { 
      "" => " ".black.on_black, 
      "0" => "0".light_red.on_black, 
      "1" => "1".light_green.on_black, 
      "2" => "2".light_yellow.on_black, 
      "3" => "3".light_blue.on_black, 
      "4" => "4".light_magenta.on_black, 
      "5" => "5".light_cyan.on_black, 
      "6" => "6".light_white.on_black
    }

    # guess arr
    # system "clear"
    print "-" * @board_width + "\n"
    for i in (0...@num_guesses)  
      arr = @guess_arr[i]
      arr.each do |g|
        g = g.to_s
        num = num_map[g] 
        num = num_map[""] if num == nil
        print "  #{num}  |"
      end

      # match arr
      print "|||" # match line 1
      arr = @match_arr[i]
      for j in (0...(arr.length/2))
        print "#{arr[j].to_s.center(1)}|"
      end
      print "|||"

      puts # match line 2

      str = " " * 5 + "|"
      print str * 4
      print "|||"
      for j in ((arr.length/2)...arr.length)
        print "#{arr[j].to_s.center(1)}|"
      end
      print "|||"
      puts
      print "-" * @board_width + "\n"
    end
  end

  def full?()
    return true if @guess_arr.find_index( [nil,nil,nil,nil] ) == nil
    return false
  end

  def game_won?()
    return true if @guess_arr.any? { |arr| arr == @code }
  end

   def valid_guess?(guess)
     return false if guess.length != @code_len
     guess.split('').each do |i|
       return false if !i.to_i.between?(0,@num_colors-1)
     end
     return true
   end

  def update_board(guess)
    # find first available index
    index = @guess_arr.find_index( [nil,nil,nil,nil] )
    @guess_arr[ index ] = guess.split('').map{ |x| x.to_i }

    match = []
    code_copy = @code.dup
    guess_copy = @guess_arr[index].dup

    # get exact matches
    for i in (0...@code_len)
      if code_copy[i] == guess_copy[i]
        code_copy[i] = nil
        guess_copy[i] = nil
        match.push( "E".black.on_light_green )
      end
    end
    
    # get fuzzy and no matches
    for i in (0...@code_len)
      next if guess_copy[i] == nil
      if (found = code_copy.find_index( guess_copy[i] )) != nil # fuzzy match
        code_copy[found] = nil
        guess_copy[i] = nil
        match.push("F".black.on_light_yellow)
      else
        match.push("-".black.on_light_red)
      end
    end

    @match_arr[index] = match.shuffle
  end

end
