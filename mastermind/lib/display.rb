# frozen_string_literal: true

# rubocop:disable Layout/LineLength

# Text needed to Mastermind
module Display
  def display_intro
    "Let's play Mastermind in the console! \n\nPlayer 1 is code creater.\nPlayer 2 is guesser.\n\n"
  end

  def display_name_prompt(number)
    "What is the name of player ##{number}?"
  end

  def display_human_prompt
    'Is this player a human?'
  end

  def display_input_warning()
    "\e[31mSorry, that is an invalid answer.\e[0m"
  end

  def display_player_turn(name, code_len, num_colors)
    guess_str = "[0-#{num_colors-1}]" * code_len
    "#{name}, please enter a guess #{guess_str}" 
  end

  def display_game_winner(player, code, width)
    str = "GAME OVER! #{player.name} is the winner! Score: #{player.score}"
    str += "\n" + "=" * ((width/2)-2) + "CODE" + "=" * ((width/2)-2) + "\n"
    code.each do |x|
      str += " #{x.to_s.center(3)} |"
    end
    str += "\n" + "=" * width + "\n"
  end

  def display_round_winner(player, code, width, round)
    str = "ROUND OVER! #{player.name} is the winner of round #{round}! Score: #{player.score}"
    str += "\n" + "=" * ((width/2)-2) + "CODE" + "=" * ((width/2)-2) + "\n"
    code.each do |x|
      str += " #{x.to_s.center(3)} |"
    end
    str += "\n" + "=" * width + "\n"
  end

  def display_tie(player, code, width)
    str = "ROUND OVER! TIE! Both players have score: #{player.score}"
    str += "\n" + "=" * ((width/2)-2) + "CODE" + "=" * ((width/2)-2) + "\n"
    code.each do |x|
      str += " #{x.to_s.center(3)} |"
    end
    str += "\n" + "=" * width + "\n"
  end


  def display_tie
    "It's a draw"
  end

  def display_code_prompt( num_colors, code_len )
    guess_str = "[0-#{num_colors-1}]" * code_len
    "Enter code in format #{guess_str}"
  end
end
# rubocop:enable Layout/LineLength
