# frozen_string_literal: true

# Game need two players
class Player
  attr_reader :name, :human
  attr_accessor :score, :guesser

  def initialize(name, human, guesser)
    @name = name
    @human = human
    @guesser = guesser
    @score = 0
  end
end
