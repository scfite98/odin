

module Display

   def display_answer_prompt( name )
      puts "#{name}, please enter the answer"
   end

   def display_guess_prompt( name )
      puts "#{name}, please enter guess"
   end

   def display_game_intro()
      puts "Lets play Hangman! First player will be the guesser"
   end

   def display_player_name_prompt( guesser )
      str = guesser ? "guesser" : "answerer"
      puts "Enter name for #{str}: "
   end

   def display_player_human( guesser )
      str = guesser ? "guesser" : "answerer"
      puts "Is #{str} human? (y/n): "
   end

   def display_answer_prompt()
      puts "TODO: not always human, enter answer:"
   end

   def display_guess_prompt( name )
      puts "#{name}, enter \"save\" to save game \n or guess a letter: "
   end

   def display_answer( board )
      puts "Answer: \"#{board.answer.join()}\""
   end

   def display_winner( name )
      puts "You win, #{name}"
   end

   def display_loser( name )
      puts "You lose, #{name}"
   end

   def display_load_option()
      puts "Would you like to load saved game? (y/n):"
   end

   def display_save_file_choices(files)
      puts "Enter number corresponding to the save file you would like to load"
      index = 0

      files.each do |f|
         puts "|#{index}| : #{File.basename(f)}"
         index += 1
      end

   end


end
