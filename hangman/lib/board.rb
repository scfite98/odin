require "json"

class Board
   attr_accessor :answer, :guessed_letters, :incorrect_letters, :correct_letters

   def initialize( answer, num_wrong_guesses )
      @answer = answer
      @guessed_letters = []
      @incorrect_letters = []
      @correct_letters = []
      @num_wrong_guesses = num_wrong_guesses
   end

   def valid?()
      ret = true
      if !@incorrect_letters ||
         @incorrect_letters.length >= @num_wrong_guesses
         ret = false
         puts "#{@num_wrong_guesses} incorrect guesses"
      end
      
      return ret
   end

   def game_won?()
      ret = !@correct_letters ? false : @correct_letters.sort.join("") == @answer.uniq.sort.join("")
      return ret
   end

   def add_guess(guess)
      # don't allow duplicates, ignore them
      if !@guessed_letters.include?(guess)
         @guessed_letters.push(guess)
         if @answer.include?(guess)
            @correct_letters.push(guess)
         else
            @incorrect_letters.push(guess)
         end
      end
   end

   # creates json string from board object
   def to_json(*a)
      {
         "json_class" => self.class.name,
         "data"       => {"answer" => @answer,
                             "guessed_letters" => @guessed_letters,
                             "incorrect_letters" => @incorrect_letters,
                             "correct_letters" => @correct_letters,
                             "num_wrong_guesses" => @num_wrong_guesses}
      }.to_json(*a)
   end

   # creates board object from json string
   def self.json_create(o)
      board = new(o["data"]["answer"], o["data"]["num_wrong_guesses"])
      board.guessed_letters = o["data"]["guessed_letters"]
      board.correct_letters = o["data"]["correct_letters"]
      board.incorrect_letters = o["data"]["incorrect_letters"]
      return board
   end

   def show()
      # system( "clear" )
      width = 32
      puts "=" * width
      puts "Guessed letters: #{@guessed_letters.join(" ")}"
      for i in @answer do
         if @correct_letters.include?( i )
            print " #{i} "
         else
            print " _ "
         end
      end
      print "\n"
      puts "Remaining guesses: #{@num_wrong_guesses - @incorrect_letters.length}"
      puts "=" * width
   end
end
