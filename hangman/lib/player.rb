require_relative 'display.rb'

class Player
   attr_reader :name

   def initialize( name, guesser )
      @name = name
      @guesser = guesser
   end
   
   def guess( board )
      puts "ERROR: Superclass call, IDK"
   end

   def create_answer()
      puts "ERROR: Superclass call, IDK"
   end
   
   # creates json string from player object
   def to_json(*a)
      {
         "json_class" => self.class.name,
         "data" => {"name" => @name,
                    "guesser" => @guesser}
         
      }.to_json(*a)
   end

   # creates a game object from a json string
   def self.json_create(o)
      new(o["data"]["name"],o["data"]["guesser"])
   end
end

class ComputerPlayer < Player

   def initialize( name, guesser, dict_file )
      super(name,guesser)
      @dict_file = dict_file
   end

   def guess( board )
      puts "computer guess NYI"
   end

   def create_answer()
      puts "computer choosing random word"
      lines = []
      File.open( @dict_file, "r" )  do |f|
         lines = f.readlines( chomp: true )
      end

      answer = ""
      until answer.length >= 5 and answer.length <= 12 do
         answer = lines[rand(0...lines.length)]
      end

      return answer
   end

   # creates json string from player object
   def to_json(*a)
      {
         "json_class" => self.class.name,
         "data" => {"name" => @name,
                    "guesser" => @guesser,
                    "dict_File" => @dict_file}
         
      }.to_json(*a)
   end

   # creates a game object from a json string
   def self.json_create(o)
      new(o["data"]["name"],o["data"]["guesser"],o["data"]["dict_file"])
   end
end

class HumanPlayer < Player
   include Display

   def initialize( name, guesser )
      super
   end

   def guess( game )
      display_guess_prompt( @name )
      guess = ""
      until guess.length == 1 do
         guess = gets.chomp.downcase
         if guess == "save"
            game.save_game
         end
      end

      return guess
   end

   def create_answer()
      display_answer_prompt( name )
      return gets.chomp.downcase
   end
end
