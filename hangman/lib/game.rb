require_relative 'display.rb'
require_relative 'player.rb'
require_relative 'board.rb'

class Game
   NUM_WRONG_GUESSES = 5
   DICT_FILE = "/home/sam/projects/hangman/google-10000-english-no-swears.txt"
   include Display

   attr_reader :answer_player, :guesser_player

   def initialize( guesser_player = nil, answer_player = nil, board = nil)
      @guesser_player = guesser_player 
      @answer_player = answer_player
      @board = board
   end
   
   # creates json string from game object
   def to_json(*a)
      {
         "json_class" => self.class.name,
         "data" => {"guesser_player" => @guesser_player,
                    "answer_player" => @answer_player,
                    "board" => @board}
         
      }.to_json(*a)
   end

   # creates a game object from a json string
   def self.json_create(o)
      new(o["data"]["guesser_player"],o["data"]["answer_player"], o["data"]["board"])
   end

   def play()
      game_setup
      player_turns()

      game_conclusion( )
   end

   def game_conclusion()
      display_answer( @board )
      if @board.game_won? 
         display_winner( @guesser_player.name )
      else
         display_loser( @guesser_player.name )
      end
   end


   def get_next_save_filename( dir )
      file_num = 0

      files = Dir["#{dir}/*"]

      for i in files do 
         cur_num = File.basename( i , ".json" )[-1].to_i 
         if cur_num >= file_num
            file_num = cur_num+1
         end
      end
      
      return "#{dir}/save#{file_num}.json"
   end

   def save_game()
      json_string = to_json # call this.to_json

      Dir.mkdir( "save/" ) if !File.directory?("save/")
      
      save_file = get_next_save_filename( "save" )

      File.open( save_file, "w" ) do |f|
         f.write(json_string)
      end
      puts "Saved game as #{save_file}. Goodbye #{@guesser_player.name}"
      exit 1
   end

   def player_turns()
      until !@board.valid? || @board.game_won?
         @board.show
         guess = @guesser_player.guess( self )
         @board.add_guess(guess)
      end
   end

   # get player info
   # optionally load saved game
   # returns board object 
   def game_setup()
      input = ""
      display_game_intro
      # ask to save if we detect save file
      if Dir.exist?("save")
         display_load_option
         input = gets.chomp.downcase
         if input == "y"
            load_saved_game
         end
      end
      if input != "y"
         guesser = true
         @guesser_player = create_player(guesser)
         guesser = false
         @answer_player = create_player(guesser)
         
         answer = @answer_player.create_answer

         @board = Board.new( answer.split(""), NUM_WRONG_GUESSES )
      end
   end

   def create_player( guesser )
      display_player_name_prompt( guesser )
      name = gets.chomp.downcase

      display_player_human( guesser )
      human = gets.chomp.downcase

      if human == "y"
         return HumanPlayer.new( name, guesser )
      else
         return ComputerPlayer.new( name, guesser, DICT_FILE )
      end
   end

   def get_desired_save(dir)
      files = Dir["#{dir}/*"]

      display_save_file_choices(files)
      input = gets.chomp

      return files[input.to_i]
   end

   # creates game object from json file
   # TODO: multiple saved games
   def load_saved_game()
      json_str = ""

      save_file = get_desired_save("save")

      puts "save_file: #{save_file}"
      File.open(save_file, "r") do |f|
         json_str = f.readlines( chomp: true )
      end

      ret = JSON.parse(json_str.join(""))

      guesser_player_str = ret["data"]["guesser_player"]
      answer_player_str  = ret["data"]["answer_player"]
      board_str          = ret["data"]["board"]

      # TODO: see how to make this less yucky
      isHuman = guesser_player_str["json_class"] == "HumanPlayer" 
      @guesser_player = (isHuman) ? HumanPlayer.json_create( guesser_player_str ) : ComputerPlayer.json_create( guesser_player_str ) 

      isHuman = answer_player_str["json_class"] == "HumanPlayer" 
      @answer_player = (isHuman) ? HumanPlayer.json_create( answer_player_str ) : ComputerPlayer.json_create( answer_player_str ) 

      @board = Board.json_create(board_str)

      puts "loaded game: "
      pp self
      puts
      # ruby should GC Game object since we're already in an object
   end
end
