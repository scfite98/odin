let playerScore = 0;
let computerScore = 0;

function getRandomInRange( max ) {
   return Math.floor( Math.random() * max );
}

function computerPlay() {
   let i = getRandomInRange( 3 );
   if ( i === 2 ) return "Rock";
   if ( i === 1 ) return "Paper";
   return "Scissors";
}

// get all losing situations, handle tie before
function playerWon( playerSel, computerSel ) {
   switch( playerSel ) {
      case "rock":
         if ( computerSel === "paper" ) return false;
         break;
      case "paper":
         if ( computerSel === "scissors" ) return false;
         break;
      case "scissors":
         if ( computerSel === "rock" ) return false;
         break;
   }
   return true;
}

function playRound( e ) {
   let playerSel = this.textContent;
   let computerSel = computerPlay();

   let player   = playerSel.toLowerCase();
   let computer = computerSel.toLowerCase();
   let decision = "";
   let win      = "";
   let lose     = "";

   if ( player === computer ) {
      const div = document.querySelector('#results');
      div.textContent = `Tie! ${playerSel} === ${computerSel}`;
      return;
   }
   if ( playerWon( player, computer )  ) {
      decision = "win";
      win      = playerSel;
      lose     = computerSel;
      playerScore += 1;
   }
   else {
      decision = "lose";
      win      = computerSel;
      lose     = playerSel;
      computerScore += 1;
   }
   const results = document.querySelector('#results');
   results.textContent = `You ${decision}! ${win} beats ${lose}`;

   const score = document.querySelector('#score');
   score.textContent = `player ${playerScore} : ${computerScore} computer`;


   if ( playerScore === 5 ||  computerScore === 5 ) {
      let winner = (playerScore > computerScore) ? "player" : "computer";
      let finalMessage = document.createElement("div");
      finalMessage.textContent = `${winner} wins the whole thing!`;
      results.appendChild( finalMessage );

      playerScore = 0;
      computerScore = 0;
      score.textContent = `player ${playerScore} : ${computerScore} computer`;
   }
}

const buttons = Array.from(document.querySelectorAll('button'));
buttons.forEach( button => button.addEventListener( 'click', playRound ) );
