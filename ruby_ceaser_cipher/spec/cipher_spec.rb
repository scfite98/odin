#spec/cipher_spec.rb
require '../lib/cipher.rb'

describe Cipher do # collection of tests ( test suite? ), either class or string argument

   let(:message1) { "What a string!" }
   let(:message2) { "a quick brown fox jumps over the lazy dog" }

   describe "#get_cipher" do   # method example group, # == instance method, . == class method
      it "shifts \"#{:message1}\" 5" do
         cipher = Cipher.new( message1, 5 )
         expect(cipher.get_cipher()).to eql("Bmfy f xywnsl!")
      end

      it "shifts \"#{:message1}\" -5" do
         cipher = Cipher.new( message1, -5 )
         expect(cipher.get_cipher()).to eql("Rcvo v nomdib!")
      end

      it "shifts \"#{:message2}\" 5" do
         cipher = Cipher.new( message2, 5 )
         expect(cipher.get_cipher()).to eql("f vznhp gwtbs ktc ozrux tajw ymj qfed itl")
      end

      it "shifts \"#{:message2}\" -5" do
         cipher = Cipher.new( message2, -5 )
         expect(cipher.get_cipher()).to eql("v lpdxf wmjri ajs ephkn jqzm ocz gvut yjb")
      end

      it "shifts \"#{:message2}\" 15" do
         cipher = Cipher.new( message2, 15 )
         expect(cipher.get_cipher()).to eql("p fjxrz qgdlc udm yjbeh dktg iwt apon sdv")
      end

      it "shifts \"#{:message2}\" -15" do
         cipher = Cipher.new( message2, -15 )
         expect(cipher.get_cipher()).to eql("l bftnv mczhy qzi ufxad zgpc esp wlkj ozr")
      end


   end
end

