#!/usr/bin/env ruby


def is_letter?( char )
  ord = char.ord;
  if ord >= 97 && ord <= 122
    return true
  else
    return false
  end
end

def shift_letter( letter, shift )
  shifted = letter.ord + shift
  if shifted > 122
    shifted = 96 + (shifted-122);
  elsif  shifted < 97
    shifted = 123 - (97-shifted);
  end
  return shifted.chr
end


def get_cipher( message, shift )
  lower_message = message.downcase;
  cipher = message.chars.map { |char|
    unless is_letter?(char.downcase) 
      char
    else
      shifted = shift_letter( char.downcase, shift );
      if char == char.upcase
        shifted.upcase
      else
        shifted
      end
    end
  }
  return cipher.join;
end


def main()
  if ARGV.length != 2
    puts "usage: <string> <cipher shift>"
    exit 1
  end

  message = ARGV[0].to_s;
  shift   = ARGV[1].to_i;

  new_message = get_cipher( message, shift );
  print( new_message + "\n");
end

# old habits
main
