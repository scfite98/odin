const body = document.querySelector('body');

let etch = document.createElement('div');
etch.setAttribute('id','etch');
etch.style.setProperty('--rows', '16');
etch.style.setProperty('--cols', '16');

function changeColor( e ) {
   e.target.style.backgroundColor = "red";
}

function createEtch( rows, cols, container ) {
   for( let j = 0; j < rows; j++ ) {
      for( let i = 0; i < cols; i++ ) {
         let outer = document.createElement('div');  
         let tmp = `${j}outer${i}`
         outer.setAttribute('id',tmp);
         outer.addEventListener("mouseenter", changeColor);

         let inner = document.createElement('div');
         tmp = `${j}inner${i}`
         inner.setAttribute('id',tmp);
         // inner.textContent = `${j},${i}`;

         outer.appendChild(inner);
         container.appendChild(outer);
      }
   }
}

function getInput( e ) {
   let rows = -1;
   let cols = -1;
   while( cols < 0 || cols > 100 || rows < 0 || rows > 100 )
   {
      rows = prompt("Enter rows[1-100]: ", 16);
      cols = prompt("Enter cols[1-100]: ", 16);
   }
   
   // remove old board and add a new empty one
   body.removeChild( etch );
   etch = document.createElement('div');
   etch.setAttribute('id','etch');
   etch.style.setProperty('--rows', rows);
   etch.style.setProperty('--cols', cols);

   body.append(etch);

   createEtch( rows, cols, etch );
}



const butt = document.createElement('button');
butt.setAttribute('type','button');
butt.textContent = 'Set Etch Dimensions';
butt.style.setProperty('--buttWidth','32px');

butt.addEventListener("click", getInput);

// add button
body.append(butt);

// create initial board
createEtch( 16, 16, etch );

// add board to body
body.append(etch);

