#spec/calculator_spec.rb
require './lib/calculator'

describe Calculator do # collection of tests ( test suite? ), either class or string argument

   describe "#add" do   # method example group, # == instance method, . == class method
      it "returns the sum of two numbers" do # individual test, takes string argument
         calculator = Calculator.new
         expect(calculator.add(5, 2)).to eql(7)
      end

      it "returns the sum of more than two numbers" do
         calculator = Calculator.new
         expect(calculator.add(2, 5, 7)).to eql(14)
      end
  end

   describe "#subtract" do   # method example group, # == instance method, . == class method
      it "returns the differnce of two numbers" do # individual test, takes string argument
         calculator = Calculator.new
         expect(calculator.subtract(5, 2)).to eql(3)
      end

      it "returns the difference of more than two numbers" do
         calculator = Calculator.new
         expect(calculator.subtract(2, 5, 7)).to eql(-10)
      end
  end


   describe "#multiply" do   # method example group, # == instance method, . == class method
      it "multiplies two numbers" do
         calculator = Calculator.new
         expect(calculator.multiply(20, 0)).to eql(0)
      end

      it "multiplies multiple numbers" do
         calculator = Calculator.new
         expect(calculator.multiply(20, 1, 2, 2)).to eql(80)
      end
   end

   describe "#divide" do   # method example group, # == instance method, . == class method
      it "divides two numbers" do
         calculator = Calculator.new
         expect(calculator.divide(20, 10)).to eql(2)
      end
   end


end

