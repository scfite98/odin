#lib/calculator.rb

class Calculator

   def add(*args)
      args.inject(0) { |sum,arg| sum += arg }
   end

   def subtract(first, *args)
      args.inject(first) { |sum,arg| sum -= arg }
   end

   def multiply(*args)
      args.inject(1) { |sum,arg| sum *= arg }
   end

   def divide(a,b)
      a / b
   end

end
