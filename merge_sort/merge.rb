def join_unsorted_halves( left, right, debug=false )
   ret = []
   li = 0
   ri = 0
   

   if debug
      puts "in join()"
      puts "left: #{left}"
      puts "right: #{right}"
   end

   if left == nil
      return right
   elsif right == nil
      return left
   end

   while li < left.size && ri < right.size do
      if left[li] < right[ri]
         ret.push(left[li])
         puts "adding left #{left[li]} to ret" if debug
         li += 1
      else
         ret.push(right[ri])
         puts "adding right #{right[ri]} to ret" if debug
         ri += 1
      end
   end
   # extra right
   if li == left.size && ri < right.size
      puts "extra right: #{right}, ret: #{ret}" if debug
      ret = ret + right[ri..-1]
   elsif ri == right.size && li < left.size # extra left
      puts "extra left: #{left}, ret: #{ret}" if debug
      ret = ret + left[li..-1]
   end
   # deal with potential left over values in left or right

   puts "join() returning #{ret}" if debug
   return ret
end


def merge_sort( arr, debug=false )
   if arr.size <= 1
      return arr
   end

   # split array in half
   # keep doing so until we have a bunch of arrays of size 1
   # recursive call with Larr and Rarr to split?
   l_arr = arr[0...arr.size/2]
   r_arr = arr[arr.size/2...arr.size]

   if debug
      puts "l_arr: #{l_arr}"
      puts "r_arr: #{r_arr}"
   end

   left = merge_sort( l_arr )
   right = merge_sort( r_arr )

   if debug
      puts "in merge_sort() left: #{left}"
      puts "in merge_sort() right: #{right}"
   end

   # return sorted array to join again
   sorted_arr = join_unsorted_halves( left, right, debug )
   return sorted_arr

   # when array is size 1
   #  compare it with its 'partner'
   #  rejoin array to old parent sorted
   #
   # with rejoined parent array, 
   #  take min from partner and this
   #  build sorted parent array
   #  ....
end

def remove_duplicates( dirty_arr )
   clean_arr = []
   for obj in dirty_arr do
         clean_arr.push( obj ) if clean_arr.count( obj ) == 0
   end
   return clean_arr
end


# debug = false

# arr = [ 9,1,6,7,2 ]
# print "unsorted: #{arr}"
# puts
# print "sorted:   #{merge_sort( arr, debug )}"
# puts

# arr = [ 9,1,6,7 ]
# print "unsorted: #{arr}"
# puts
# print "sorted:   #{merge_sort( arr, debug )}"
# puts


# arr = [ 1,2,4,5,9,10 ]
# print "unsorted: #{arr}"
# puts
# print "sorted:   #{merge_sort( arr, debug )}"
# puts

# arr = [ 10,9,8,1,2,3,4,9,10,11, 99 ]
# print "unsorted: #{arr}"
# puts
# print "sorted:   #{merge_sort( arr, debug )}"
# puts


# arr = [ 9,2 ]
# print "unsorted: #{arr}"
# puts
# print "sorted:   #{merge_sort( arr, debug )}"
# puts

