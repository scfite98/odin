# frozen_string_literal: true

require_relative '../lib/board.rb'
require_relative '../lib/player.rb'

# Here is a summary of what should be tested
   # 1. Command Method -> Test the change in the observable state
   # 2. Query Method -> Test the return value
   # 3. Method with Outgoing Command -> Test that a message is sent
   # 4. Looping Script Method -> Test the behavior of the method
   

# There are a handful of methods that you do not need to test:
   # 1. You do not have to test #initialize if it is only creating instance
   # variables. However, if you call methods inside the initialize method, you
   # might need to test #initialize and/or the inside methods. In addition, you
   # will need to stub any inside methods because they will be called when you
   # create an instance of the class.
   # 2. You do not have to test methods that only contain 'puts' or 'gets' 
   # because they are well-tested in the standard Ruby library.
   # 3. Private methods do not need to be tested because they should have test
   # coverage in public methods. However, as previously discussed, you may have
   # some private methods that are called inside a script or looping script method;
   # these methods should be tested publicly.


describe Board do
   subject(:board) { described_class.new(3) }

   describe '#valid_turn?' do
      # Query Method -> Test the return value

      context 'when row is too large' do
         it 'returns false' do
            height = board.instance_variable_get( :@height )
            expect( board.valid_turn?( [0,height+1] ) ).to be( false )
         end
      end

      context 'when col is too large' do
         it 'returns false' do
            width = board.instance_variable_get( :@width )
            expect( board.valid_turn?( [width+1,0] ) ).to be( false )
         end
      end

      context 'when board is new and move is on board' do
         it 'returns true' do
            expect( board.valid_turn?( [0,0] ) ).to be( true )
         end
      end

      context 'when board has been played and move is open' do
         before do
            arr = [[-1, "X", "O"],
                   ["X", "O", "X"],
                   ["X", "X", "O"]]
            board.instance_variable_set( :@board_arr, arr )
         end
         it 'returns true' do
            expect( board.valid_turn?( [0,0] ) ).to be( true )
         end
      end

      context 'when board has been played and move is not open' do
         before do
            arr = [[-1, "X", "O"],
                   ["X", "O", "X"],
                   ["X", "X", "O"]]
            board.instance_variable_set( :@board_arr, arr )
         end
         it 'returns false' do
            expect( board.valid_turn?( [0,1] ) ).to be( false )
         end
      end
   end

   describe '#add_turn' do
   # 1. Command Method -> Test the change in the observable state
      context 'when board is new' do
         it 'successfully adds the turn' do
            expected_arr = [["X", -1, -1],
                            [-1,  -1, -1],
                            [-1,  -1, -1]]

            player = Player.new( "X" )
            board.add_turn( [0,0], player )

            received_arr = board.instance_variable_get( :@board_arr )
            expect( received_arr ).to eq( expected_arr )
         end
      end

      context 'when board has been played and spot taken' do
         starting_arr = nil
         before do
            starting_arr = [["X", -1, -1],
                           [-1,  -1, -1],
                           [-1,  -1, -1]]
            board.instance_variable_set( :@board_arr, starting_arr )
         end

         it 'doesn\'t add the turn' do
            error_msg = "ERROR adding turn row 0 col 0"
            expect( board ).to receive(:puts).once.with( error_msg )

            player = Player.new( "X" )
            board.add_turn( [0,0], player )

            received_arr = board.instance_variable_get( :@board_arr )
            expect( received_arr ).to eq( starting_arr )
         end
      end
   end

   describe '#game_won?' do
   # 2. Query Method -> Test the return value
      context 'when the board is new' do
         it 'returns false' do
            expect( board.game_won?( "X", [0,0] ) ).to be( false )
         end
      end

      context 'when a winning vertical move is made, column 0' do
         before do
            starting_arr = [["X", -1, -1],
                            ["X", -1, -1],
                            ["X", -1, -1]]
            board.instance_variable_set( :@board_arr, starting_arr )
         end

         it 'returns true' do
            expect( board.game_won?( "X", [0,0] ) ).to be( true )
         end
      end

      context 'when a winning vertical move is made, column 2' do
         before do
            starting_arr = [[-1, -1, "O"],
                            [-1, -1, "O"],
                            [-1, -1, "O"]]
            board.instance_variable_set( :@board_arr, starting_arr )
         end

         it 'returns true' do
            expect( board.game_won?( "O", [0,2] ) ).to be( true )
         end
      end

      context 'when a winning horizontal move is made, row 0' do
         before do
            starting_arr = [["X", "X", "X"],
                            [-1, -1, -1],
                            [-1, -1, -1]]
            board.instance_variable_set( :@board_arr, starting_arr )
         end

         it 'returns true' do
            expect( board.game_won?( "X", [0,0] ) ).to be( true )
         end
      end

      context 'when a winning horizontal move is made, row 2' do
         before do
            starting_arr = [[-1, -1, -1],
                            [-1, -1, -1],
                            ["O", "O", "O"]]
            board.instance_variable_set( :@board_arr, starting_arr )
         end

         it 'returns true' do
            expect( board.game_won?( "O", [2,0] ) ).to be( true )
         end
      end

      context 'when a winning diagonal move is made, top-left -> bottom-right' do
         before do
            starting_arr = [["X", -1, -1],
                            [-1, "X", -1],
                            [-1, -1, "X"]]
            board.instance_variable_set( :@board_arr, starting_arr )
         end

         it 'returns true' do
            expect( board.game_won?( "X", [0,0] ) ).to be( true )
         end
      end

      context 'when a winning diagonal move is made, top-right -> bottom-left' do
         before do
            starting_arr = [[-1, -1, "O"],
                            [-1, "O", -1],
                            ["O", -1, -1]]
            board.instance_variable_set( :@board_arr, starting_arr )
         end

         it 'returns true' do
            expect( board.game_won?( "O", [2,0] ) ).to be( true )
         end
      end

      context 'when there is a tie' do
         before do
            @starting_arr = [["O", "X", "O"],
                             ["X", "O", "X"],
                             ["X", "O", "X"]]
            board.instance_variable_set( :@board_arr, @starting_arr )
         end

         it 'returns false for each position for each player' do
            @starting_arr.each_with_index do |elem, i|
               elem.each_with_index do |player, j|
                  expect( board.game_won?( "#{player}", [i,j] ) ).to be( false )
               end
            end
         end
      end
   end

   describe '#full?' do
      context 'when the board is empty' do
         before do
            @starting_arr = [[-1, -1, -1],
                            [-1, -1, -1],
                            [-1, -1, -1]]
            board.instance_variable_set( :@board_arr, @starting_arr )
         end

         it 'returns false' do
            expect( board.full?() ).to be( false )
         end
      end

      context 'when the board is almost full' do
         before do
            @starting_arr = [["O", "X", "O"],
                            ["X", "O", "X"],
                            ["X", "O", -1]]
            board.instance_variable_set( :@board_arr, @starting_arr )
         end

         it 'returns false' do
            expect( board.full?() ).to be( false )
         end
      end

      context 'when the board is full' do
         before do
            @starting_arr = [["O", "X", "O"],
                             ["X", "O", "X"],
                             ["X", "O", "X"]]
            board.instance_variable_set( :@board_arr, @starting_arr )
         end

         it 'returns true' do
            expect( board.full?() ).to be( true )
         end
      end




   end
end
