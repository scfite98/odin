# frozen_string_literal: true

# ttt board, will hold all state and methods 
class Board

   attr_reader :width, :height
   def initialize( size=3 )
      @board_arr = Array.new( size ) { 
         Array.new( size, -1 ) 
      }
      @width     = size 
      @height    = @width
   end

   def valid_turn?( turn )
      row = turn[0]
      col = turn[1]
      return false if ( row < 0 or row >= @board_arr.length() )
      return false if ( col < 0 or col >= @board_arr[0].length() )
      return false if ( @board_arr.at(row).at(col) != -1 )
      return true
   end

   def add_turn(turn, player)
      row = turn[0]
      col = turn[1]
      if ( self.valid_turn?( turn ) )
         @board_arr[row][col] = player.symbol
      else
         puts "ERROR adding turn row #{row} col #{col}"
      end
   end

   def game_won?( debug=false, current_player_symbol, turn )
      row = turn[0]
      col = turn[1]

      pp turn if debug
      pp current_player_symbol if debug

      # check vertical
      count = @board_arr[0...@height].inject(0) do |count, row| 
         (row[col]== current_player_symbol) ? count + 1 : count 
      end
      return true if count == @height
      puts count if debug

      # check horizontal
      count = @board_arr[row].inject(0) do |count, player| 
         (player == current_player_symbol) ? count + 1 : count 
      end
      return true if count == @width
      puts count if debug

      # check diagnal top left -> bottom right
      next_row, next_col = 0, 0
      count = 0
      while next_row >= 0 and next_col >= 0 and next_row < @height and next_col < @width do
         count += 1 if @board_arr[next_row][next_col] == current_player_symbol
         next_row += 1
         next_col += 1
      end
      return true if count == @width
      puts count if debug

      # check diagnal top right -> bottom left
      next_row, next_col = 0, 2
      count = 0
      while next_row >= 0 and next_col >= 0 and next_row < @height and next_col < @width do
         count += 1 if @board_arr[next_row][next_col] == current_player_symbol
         next_row += 1
         next_col -= 1
      end
      return true if count == @width
      puts count if debug
 
      return false
   end

   def full?()
      return !(@board_arr.flatten().any?{ |elem| elem == -1 })
   end

   def show_available_indexes()
      @board_arr.each_index do |i| 
         @board_arr[i].each_index do |j| 
            if @board_arr[i] == -1
               print "#{i.to_s} #{j.to_s}| " 
            end
         end
         puts ""
      end
   end

   def show()
      @board_arr.each_index do |i| 
         @board_arr[i].each_index do |j| 
            cell = @board_arr[i][j] == -1 ? " " : @board_arr[i][j]
            sep  = "|" 
            if ( j+1 == @width )
               sep = ""
            end
            line = ""
            if ( j+1 == @width && i+1 < @height )
               line = "\n----------\n"
            elsif ( j+1 == @width && i+1 == @height )
               line = "\n"
            end
            print "#{cell} #{sep} #{line}"
         end
      end
   end

end

