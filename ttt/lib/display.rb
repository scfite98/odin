# frozen_string_literal: true

module  Display
   def display_prompt_turn( player_symbol, min_height, min_width, max_height, max_width )
      puts "Enter move for player #{player_symbol}: \"row col\":(row[#{min_height}-#{max_height}] col[#{min_width}-#{max_width}])"
   end

   def display_win( current_player_symbol )
      puts "Player #{current_player_symbol} WINS!"
   end

   def display_invalid_input()
      puts "Invalid coordinate, available coordinates: "
   end

   def display_tie()
      puts "TIE GAME!"
   end
end
