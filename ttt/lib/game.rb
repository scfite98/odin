# frozen_string_literal: true

require_relative 'display.rb'
require_relative 'board.rb'
require_relative 'player.rb'

class Game
   include Display

   attr_reader :board

   def initialize()
      @player_one = Player.new( "X" )
      @player_two = Player.new( "O" )
      @current_player = @player_one
      @board = Board.new( 3 )
   end

   def play()
      while 1 do
         turn = self.get_turn()
         @board.add_turn( turn, @current_player )
         @board.show()

         debug = false
         if @board.game_won?( debug, @current_player.symbol, turn )
            display_win( @current_player.symbol )
            break
         elsif @board.full?
            display_tie()
            break
         end

         self.update_current_player()
      end
   end

private

      def get_turn()
         turn = nil
         while 1 do
            display_prompt_turn(@current_player.symbol, 0, 0, @board.width-1, @board.height-1)
            input = gets.chomp.split(" ")
            turn = [input[0].to_i, input[1].to_i]
            break if @board.valid_turn?( turn )
            display_invalid_input()
            @board.show_available_indexes()
         end
         return turn
      end

      def update_current_player()
         @current_player = @current_player.equal?(@player_one) ? @player_two : @player_one
      end
end
