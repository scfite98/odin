# frozen_string_literal: true

require_relative 'game.rb'
require_relative 'board.rb'
require_relative 'player.rb'
require_relative 'display.rb'

def play_game()
   game = Game.new()
   game.play()
end

def repeat_game()
end

play_game()
