#!/usr/bin/env ruby


# ttt board, will hold all state and methods 
class Board
   def initialize( size=3 )
      @board_arr = Array.new( size ) { 
         Array.new( size, -1 ) 
      }
      @width     = size 
      @height    = @width
      @x_turn    = true
      @current_player = "X"
      @current_turn = [-1,-1]
   end

   def play_game()
      while 1 do
         self.get_turn()
         self.add_turn()
         self.display_board()

         debug = false
         if game_won?( debug )
            display_win()
            exit 0
         end

         self.update_turn()
      end
   end

   private

   def get_turn()
      row, col = nil, nil
      while 1 do
         self.prompt_turn()
         coordinate = gets.chomp.split(" ")
         row = coordinate[0].to_i
         col = coordinate[1].to_i
         break if self.valid_turn?( row, col )
         self.display_invalid_input()
         self.display_available_indexes()
      end
      @current_turn[0] = row.to_i
      @current_turn[1] = col.to_i
   end

   def valid_turn?( row, col )
      return false if ( row < 0 or row >= @board_arr.length() )
      return false if ( col < 0 or col >= @board_arr[0].length() )
      return false if ( @board_arr.at(row).at(col) != -1 )
      return true
   end

   def add_turn()
      row = @current_turn[0]
      col = @current_turn[1]
      if ( self.valid_turn?( row, col ) )
         @board_arr[row][col] = @x_turn ? "X" : "O"
      else
         puts "ERROR adding turn row #{row} col #{col}"
      end
   end

   def update_turn()
      @x_turn = !@x_turn
      @current_player = @x_turn ? "X" : "O"
   end

   def game_won?( debug=false )
      row = @current_turn[0]
      col = @current_turn[1]

      # check vertical
      count = @board_arr[0...@height].inject(0) do |count, row| 
         (row[col]== @current_player) ? count + 1 : count 
      end
      return true if count == @height

      # check horizontal
      count = @board_arr[row].inject(0) do |count, player| 
         (player == @current_player) ? count + 1 : count 
      end
      return true if count == @width

      # check diagnal top left -> bottom right
      next_row, next_col = 0, 0
      count = 0
      while next_row >= 0 and next_col >= 0 and next_row < @height and next_col < @width do
         count += 1 if @board_arr[next_row][next_col] == @current_player
         next_row += 1
         next_col += 1
      end
      return true if count == @width

      # check diagnal top right -> bottom left
      next_row, next_col = 0, 2
      count = 0
      while next_row >= 0 and next_col >= 0 and next_row < @height and next_col < @width do
         count += 1 if @board_arr[next_row][next_col] == @current_player
         next_row += 1
         next_col -= 1
      end
      return true if count == @width
 
      return false
   end


   def prompt_turn()
      player = @x_turn ? "X" : "O"
      puts "Enter move for player #{player}: \"row col\":(row[0-#{@height-1}] col[0-#{@width-1}])"
   end

   def display_win()
      puts "Player #{@current_player} WINS!"
   end

   def display_invalid_input()
      puts "Invalid coordinate, available coordinates: "
   end
  
   def display_available_indexes()
      @board_arr.each_index do |i| 
         @board_arr[i].each_index do |j| 
            if @board_arr[i] == -1
               print "#{i.to_s},#{j.to_s}| " 
            end
         end
         puts ""
      end
   end

   def display_board()
      @board_arr.each_index do |i| 
         @board_arr[i].each_index do |j| 
            cell = @board_arr[i][j] == -1 ? " " : @board_arr[i][j]
            sep  = "|" 
            if ( j+1 == @width )
               sep = ""
            end
            line = ""
            if ( j+1 == @width && i+1 < @height )
               line = "\n----------\n"
            elsif ( j+1 == @width && i+1 == @height )
               line = "\n"
            end
            print "#{cell} #{sep} #{line}"
         end
      end
   end

end


Board.new().play_game()
