# frozen_string_literal: true

class Player
   attr_reader :symbol
   def initialize( symbol )
      @symbol = symbol
   end
end
