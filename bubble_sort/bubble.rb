#!/usr/bin/env ruby







def is_sorted?( arr )
   for i in (0...arr.length-1)
      if arr[i] > arr[i+1]
         return false
      end
   end
   return true
end

def bubble_sort( arr )

   index = 0
   sorted = is_sorted?( arr )
   until sorted == true
      #dont walk off
      if index == arr.length-1
         index = 0
         next
      end
      #swap
      if arr[index] > arr[index+1]
         tmp = arr[index]
         arr[index] = arr[index+1]
         arr[index+1] = tmp
      end
      index += 1
      sorted = is_sorted?( arr )
   end

   return arr
end

p bubble_sort([4,3,78,2,0,2,1,6,3,99,3,5,6,3,1,6,7,8,8,4,633,456,3456,345,634,6,1,34,9])
