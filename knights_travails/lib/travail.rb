TRANSFORMATIONS = [[1, 2], [-2, -1], [-1, 2], [2, -1],
                     [1, -2], [-2, 1], [-1, -2], [2, 1]].freeze

def get_possible_moves( src )
   TRANSFORMATIONS.map{ |t| [src[0]+t[0], src[1]+t[1]] }
                  .keep_if{ |e| e[0].between?(0,7) && e[1].between?(0,7) }
end


def knight_moves( src, dest )
   
   queue = Queue.new
   queue.push( [src] )
   while not queue.empty?
      path = queue.pop()
      square = path[-1]
      puts "DUPLICATE: #{path}" if path.count(square) > 1
      return path if square == dest
      
      moves = get_possible_moves( square )
      for i in moves do
         next if path.find_index( i ) != nil # skip visited squares
         new_path = Array.new( path )
         new_path.push( i )
         queue.push( new_path )
      end
   end

   return path
end

for i in (0...8)
   for j in (0...8)
      for k in (0...8)
         for l in (0...8)
            pp knight_moves( [i,j], [k,l] )
         end
      end
   end
end
