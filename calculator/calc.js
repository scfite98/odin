const body = document.querySelector('body');
let display = document.querySelector('#display');
let calc = document.querySelector('#calc');
let inputs = []; 


function add( a, b ) {
   return Number(a) + Number(b);
}

function subtract( a, b ) {
   return Number(a) - Number(b);
}

function multiply( a, b ) {
   return Number(a) * Number(b);
}

function divide( a, b ) {
   return Number(a) / Number(b);
}

function operate( operator, a, b ) {
   switch( operator ) {
      case "+":
         return add( a, b );
      case "-":
         return subtract( a, b );
      case "*":
         return multiply( a, b );
      case "/":
         return divide( a, b );
      default:
         return "ERROR";
   }
}

function isOperator( i ) {
   switch( i ) {
      case "/":
         return true;
      case "*":
         return true;
      case "+":
         return true;
      case "-":
         return true;
      default:
         return false;
   }
}

function updateDisplay( input, clear ) {
   let copy = display;
   calc.removeChild(display);

   if ( clear == true ) {
      copy.textContent = input;
   } else {
      copy.textContent = copy.textContent + input;
   }
   calc.append( copy );
}

function handleOperator( input, len ) {
   let value = input;
   // add operator if it is second token 
   if ( len == 1 ) {
      inputs.push( input );
   }
   // if operator is fourth input, evaluate previous set and
   //  add to new input array
   else if ( len == 3 ){ 
      let res = operate( inputs[1], inputs[0], inputs[2] );
      updateDisplay( res, true );
      // update array with previously evaluated expr and supplied operator
      inputs = [ res, input ];
      value = res;
   }
   // otherwise replace operator with new one
   else {
      inputs[1] = input;
   }
   updateDisplay( value, true );
}

function handleClear( input, len ) {
   inputs = [];
   updateDisplay( "", true );
}

function handleEnter( input, len ) {
   let res = operate( inputs[1], inputs[0], inputs[2] );
   updateDisplay( res, true );
   inputs = [];
}

function handleNumber( input, len ) {
   let bucket = len;
   // clear screen if number comes after operator
   let clear = ( len == 2 ) ? true : false; 

   if ( len == 0 || len == 2 ){ 
      bucket++; 
      // initialize to empty string not 'undefined'
      inputs[bucket-1] = "";
   }
   // concatenate number 
   inputs[ bucket-1 ] = inputs[ bucket-1 ] + input; 
   updateDisplay( input, clear );
}

// add input to our data stucture
function addInput( input ) {
   let len = inputs.length;

   if ( isOperator( input ) ) { 
      handleOperator( input, len );
   } else if ( input === 'clear' ) { 
      handleClear( input, len );
   } else if ( input === 'enter' ) { 
      handleEnter( input, len );
   } else {
      handleNumber( input, len );
   }
}

function zero( e ) { 
   addInput( '0' );
}

function one( e ) { 
   addInput( '1' );
}

function two( e ) { 
   addInput( '2' );
}

function three( e ) { 
   addInput( '3' );
}

function four( e ) { 
   addInput( '4' );
}

function five( e ) { 
   addInput( '5' );
}

function six( e ) { 
   addInput( '6' );
}

function seven( e ) { 
   addInput( '7' );
}

function eight( e ) { 
   addInput( '8' );
}

function nine( e ) { 
   addInput( '9' );
}

function slash( e ) { 
   addInput( '/' );
}

function star( e ) { 
   addInput( '*' );
}

function minus( e ) { 
   addInput( '-' );
}

function plus( e ) { 
   addInput( '+' );
}

function enter( e ) { 
   addInput( 'enter' );
}

function dot( e ) { 
   addInput( '.' );
}

function clear( e ) { 
   addInput( 'clear' );
}

// add event listeners for all of the buttons
let buttons = document.querySelectorAll('button');

for ( let i = 0; i < buttons.length; i++ ) {
   let fn = this[buttons[i].id];
   buttons[i].addEventListener('click', fn );
}
